import React, {Component} from 'react';
import './App.css';
import Messages from './containers/Messages/Messages';
import PostMessage from './containers/PostMessage/PostMessage';
import { PageHeader, Label } from 'react-bootstrap';

class App extends Component {
    render() {
        return (
            <main className="container">
                <PageHeader>
                    <Label bsStyle="primary">Messages</Label>
                </PageHeader>
                <div className="main-desk">
                    <Messages />
                    <PostMessage/>
                </div>
            </main>
        );
    }
}

export default App;
