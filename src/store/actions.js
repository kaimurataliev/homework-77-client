import axios from '../axios';

export const FETCH_MESSAGES = "FETCH_MESSAGES";
export const FETCH_ERROR = "FETCH_ERROR";
export const POST_MESSAGE_SUCCESS = "POST_MESSAGE_SUCCESS";

export const fetchMessagesSuccess = data => {
    return {type: FETCH_MESSAGES, data}
};

export const fetchError = (data) => {
    return {type: FETCH_ERROR, data}
};

export const postMessageSuccess = data => {
    return {type: POST_MESSAGE_SUCCESS, data}
};


export const fetchMessages = () => {
    return (dispatch) => {
        return axios.get('/messages').then(response => {
            dispatch(fetchMessagesSuccess(response.data))
        })
    }
};

export const postMessage = (message) => {
    return (dispatch) => {
        return axios.post('/messages', message).then((res) => {
            dispatch(postMessageSuccess(res.data));
        }, error => {
            dispatch(fetchError(error.response.data.error))
        });
    }
};