import * as actions from './actions';

const initialState = {
    messages: [],
    error: ''
};

const reducer = (state = initialState, action) => {
    switch(action.type) {
        case actions.FETCH_MESSAGES:
            return {...state, messages: action.data, error: ''};

        case actions.FETCH_ERROR:
            return {...state, error: action.data};

        case actions.POST_MESSAGE_SUCCESS:
            let messages = [...state.messages];
            messages.push(action.data);
            return {...state, messages: messages, error: ''};

        default:
            return state
    }
};

export default reducer;