import React, { Component, Fragment } from 'react';
import { FormGroup, FormControl, Form, ControlLabel, Alert } from 'react-bootstrap';
import { connect } from 'react-redux';
import Button from "react-bootstrap/es/Button";
import { fetchMessages, postMessage} from "../../store/actions";

class PostMessage extends Component {

    state = {
        author: '',
        message: '',
        image: ''
        // error: false
    };

    fileChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.files[0]
        });
    };

    getMessage = (event) => {
        this.setState({message: event.target.value});

    };


    getAuthor = (event) => {
        this.setState({author: event.target.value});
    };

    sendHandler = (event) => {
        event.preventDefault();

        const formData = new FormData();
        Object.keys(this.state).forEach((key) => {
            formData.append(key, this.state[key])
        });

        this.props.onSendMessage(formData);

    };

    render() {
        return (
            <Fragment>
                <Form style={{width: 600}} required onSubmit={this.sendHandler}>
                    <FormGroup>

                        {this.props.error ? <Alert bsStyle="danger"><strong>You have to enter message</strong></Alert> : null}

                        <ControlLabel>Enter author</ControlLabel>
                        <FormControl
                            type="text"
                            placeholder="Enter name"
                            onChange={this.getAuthor}
                        />

                        <ControlLabel>Enter message</ControlLabel>
                        <FormControl
                            required
                            style={{marginBottom: 10}}
                            type="text"
                            name="message"
                            placeholder="Enter message"
                            onChange={this.getMessage}
                        />

                        <ControlLabel>Choose photo</ControlLabel>
                        <FormControl
                            style={{marginBottom: 10}}
                            type="file"
                            name="image"
                            onChange={this.fileChangeHandler}
                        />
                        <Button bsStyle="primary" type="submit">Send</Button>
                    </FormGroup>
                </Form>
            </Fragment>
        )
    }
}

const mapStateToProps = state => {
    return {
        error: state.error
    }
};


const mapDispatchToProps = (dispatch) => {
    return {
        onSendMessage: (message) => dispatch(postMessage(message)),
        fetchMessages: () => dispatch(fetchMessages()),
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(PostMessage);