import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import {fetchMessages} from "../../store/actions";
import { Panel, Image, Alert } from 'react-bootstrap';

class Messages extends Component {

    componentDidMount () {
        this.props.fetchMessages();
    }

    render() {
        return (
            <Fragment>
                {this.props.messages.length === 0 ? <Alert><strong>No messages yet</strong></Alert> : this.props.messages.map((message) => {
                    return (
                        <Panel style={{width: 600}} key={message.id}>
                            <Panel.Body>
                                {message.image &&
                                <Image
                                    thumbnail
                                    style={{width: '100px', marginRight: '100px'}}
                                    src={'http://localhost:8000/uploads/' + message.image}
                                />
                                }
                                <p>Author: <strong>{message.author}</strong></p>
                                <p>Message: <strong>{message.message}</strong></p>
                            </Panel.Body>
                        </Panel>
                    )
                })}
            </Fragment>
        )
    }
}

const mapStateToProps = state => {
    return {
        messages: state.messages
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        fetchMessages: () => dispatch(fetchMessages())
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Messages);